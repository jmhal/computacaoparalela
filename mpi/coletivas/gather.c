#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
   int  numtasks, rank, len, rc; 

   // Inicializar o Ambiente.  
   MPI_Init(&argc,&argv);

   // Recuperar o número de Processos.
   MPI_Comm_size(MPI_COMM_WORLD, &numtasks);

   // Recuperar o identificador do processo.
   MPI_Comm_rank(MPI_COMM_WORLD, &rank);

   // Define um vetor local
   int vetorLocal[10];
   for (int i = 0; i < 10; i++)
      vetorLocal[i] = rank;
   
   int *vetorFinal;
   if (rank == 0) {
      vetorFinal = (int *) malloc(10 * numtasks * sizeof(int));
   }
   
   MPI_Gather(vetorLocal, 10, MPI_INT, vetorFinal, 10, MPI_INT, 0, MPI_COMM_WORLD);

   if (rank == 0) {
      for (int i = 0; i < numtasks * 10; i++)
         printf("%d ", vetorFinal[i]);
      printf("\n");         
      free(vetorFinal);
   }

   // Finalizar o ambiente. 
   MPI_Finalize();
}

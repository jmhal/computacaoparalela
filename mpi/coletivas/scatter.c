#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char *argv[]) {
   int  numtasks, rank, len, rc; 

   // Tamanho do vetor a ser distribuído.
   int tamanhoVetorGlobal = atoi(argv[1]);

   // Inicializar o Ambiente.  
   MPI_Init(&argc,&argv);

   // Recuperar o número de Processos.
   MPI_Comm_size(MPI_COMM_WORLD, &numtasks);

   // Recuperar o identificador do processo.
   MPI_Comm_rank(MPI_COMM_WORLD, &rank);

   // Define um vetor local
   int *vetorLocal;
   vetorLocal = (int *) malloc(tamanhoVetorGlobal/numtasks * sizeof(int));
   
   // Vetor a ser distribuído.
   int *vetorGlobal;
   char *output;
   if (rank == 0) {
      vetorGlobal = (int *) malloc(tamanhoVetorGlobal * sizeof(int));
      output = (char *) malloc((tamanhoVetorGlobal * 5 + 100) * sizeof(char));
      srand(time(0));
      sprintf(output, " "); 
      for (int i = 0; i < tamanhoVetorGlobal; i++) {
         vetorGlobal[i] = rand() % 100;
         sprintf(output, "%s%d ", output,  vetorGlobal[i]);
      }
      printf("Rank 0 distribui: %s\n", output);
      free(output);
   }
  
   // Realiza a distribuição.
   MPI_Scatter(vetorGlobal, tamanhoVetorGlobal/numtasks, MPI_INT, 
               vetorLocal, tamanhoVetorGlobal/numtasks, MPI_INT, 
               0, MPI_COMM_WORLD);

   // Cada processo imprime sua parte.
   output = (char *) malloc(((tamanhoVetorGlobal/numtasks) * 5 + 100) * sizeof(char));
   sprintf(output, "Rank %d: ", rank);
   for (int i = 0; i < tamanhoVetorGlobal/numtasks; i++)
      sprintf(output, "%s%d ", output,  vetorLocal[i]);
   printf("%s\n", output);
   free(output);
   free(vetorLocal);
   if (rank == 0) {
      free(vetorGlobal);
   }

   // Finalizar o ambiente. 
   MPI_Finalize();
}

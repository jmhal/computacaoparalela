#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
   int  numtasks, rank, len, rc; 

   // Inicializar o Ambiente.  
   MPI_Init(&argc,&argv);

   // Recuperar o número de Processos.
   MPI_Comm_size(MPI_COMM_WORLD, &numtasks);

   // Recuperar o identificador do processo.
   MPI_Comm_rank(MPI_COMM_WORLD, &rank);

   // Define um vetor local. Ele terá tamanho rank + 1 
   // (lembre que rank pode ser 0) e armazenará o valor do rank.
   int *vetorLocal;
   vetorLocal = (int *) malloc((rank + 1) * sizeof(int));
   for (int i = 0; i < (rank + 1); i++)
      vetorLocal[i] = rank;

   // Definir quantos valores irá receber de cada processo.
   int *recvcounts;
   recvcounts = (int *) malloc(numtasks * sizeof(int));
   for (int i = 0; i < numtasks; i++) {
      // De cada processo, ele receberá rank + 1 valores.
      recvcounts[i] = i + 1;
   }
   
   // Definir o deslocamento no vetorFinal
   int *displs;
   displs = (int *) malloc(numtasks * sizeof(int));
   displs[0] = 0;
   for (int i = 1; i < numtasks; i++) {
      displs[i] = i + displs[i-1];
   }
  
   // O vetor final, alocado apenas no rank 0.
   int *vetorFinal;
   int vetorFinalTamanho = displs[numtasks - 1] + numtasks;
   if (rank == 0) {
      vetorFinal = (int *) malloc(vetorFinalTamanho * sizeof(int));
   }
   
   MPI_Gatherv(vetorLocal, rank+1, MPI_INT, vetorFinal, recvcounts, displs, MPI_INT, 0, MPI_COMM_WORLD);

   if (rank == 0) {
      for (int i = 0; i < vetorFinalTamanho; i++)
         printf("%d ", vetorFinal[i]);
      printf("\n");         
      free(vetorFinal);
   }

   // Finalizar o ambiente. 
   MPI_Finalize();
}

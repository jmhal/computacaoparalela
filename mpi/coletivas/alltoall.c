#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>

int printArrayForRank(int rank, int *array,  int arraySize, char *output) {
   int i;
   sprintf(output, "Rank %d: ", rank);
   for (int i = 0; i < arraySize; i++)
      sprintf(output, "%s %d", output, array[i]);
   sprintf(output,"%s\n", output);
   return i;
}

int main(int argc, char *argv[]) {
   int  numtasks, rank, len, rc; 

   // Inicializar o Ambiente.  
   MPI_Init(&argc,&argv);

   // Recuperar o número de Processos.
   MPI_Comm_size(MPI_COMM_WORLD, &numtasks);

   // Recuperar o identificador do processo.
   MPI_Comm_rank(MPI_COMM_WORLD, &rank);

   // Define buffers local
   int *bufferEnvioLocal = (int *) malloc(10 * numtasks * sizeof(int));
   int *bufferRecebimentoLocal = (int *) malloc(10 * numtasks * sizeof(int));
   for (int i = 0; i < 10; i++)
      bufferEnvioLocal[i] = i + 10 * rank;

   // Imprime os valores iniciais
   char output[1000];
   printArrayForRank(rank, bufferEnvioLocal, 10, output);
   printf("%s", output);

   MPI_Alltoall(bufferEnvioLocal, 10 / numtasks, MPI_INT, bufferRecebimentoLocal, 10 / numtasks, MPI_INT, MPI_COMM_WORLD);

   // Imprime os valores finais
   printArrayForRank(rank, bufferRecebimentoLocal, 10, output);
   printf("%s", output);
   
   // Finalizar o ambiente. 
   free(bufferEnvioLocal);
   free(bufferRecebimentoLocal);
   MPI_Finalize();
   return 0;
}

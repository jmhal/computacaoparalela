#include "mpi.h"
#include <stdio.h>

int printArrayForRank(int rank, int *array,  int arraySize, char *output) {
   int i;
   sprintf(output, "Rank %d: ", rank);
   for (int i = 0; i < arraySize; i++)
      sprintf(output, "%s %d", output, array[i]);
   sprintf(output,"%s\n", output);
   return i;
}

int main(int argc, char *argv[]) {
   int  numtasks, rank, len, rc; 

   // Inicializar o Ambiente.  
   MPI_Init(&argc,&argv);

   // Recuperar o número de Processos.
   MPI_Comm_size(MPI_COMM_WORLD,&numtasks);

   // Recuperar o identificador do processo.
   MPI_Comm_rank(MPI_COMM_WORLD,&rank);

   // Define uma mensagem qualquer.
   int message[10];
   char output[1000];
   if (rank == 0) {
      for (int i = 0; i < 10; i++)
         message[i] = 100;
   }   

   // Valores originais
   sprintf(output, "Rank %d: ", rank);
   for (int i = 0; i < 10; i++)
      sprintf(output, "%s %d", output, message[i]);
   sprintf(output,"%s\n", output);
   printf("Original %s", output);
   
   // Faz um broadcast
   MPI_Bcast(message, 10, MPI_INT, 0, MPI_COMM_WORLD);

   // Valores finais.
   printArrayForRank(rank, message, 10, output);
   printf("Final %s", output);
   
   // Finalizar o ambiente. 
   MPI_Finalize();
}

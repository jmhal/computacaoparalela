#include "mpi.h"
#include <stdio.h>

int main(int argc, char *argv[]) {
   int  numtasks, rank, len, rc; 

   // Inicializar o Ambiente.  
   MPI_Init(&argc,&argv);

   // Recuperar o número de Processos.
   MPI_Comm_size(MPI_COMM_WORLD, &numtasks);

   // Recuperar o identificador do processo.
   MPI_Comm_rank(MPI_COMM_WORLD, &rank);

   // Define dois vetores.
   float vetor1[100];
   float vetor2[100];
   float produtoEscalar;
   for (int i = 0; i < 100; i++) {
      vetor1[i] = 1;
      vetor2[i] = 2;
   }

   // Faz trabalho local.
   float produtoLocal = 0.0;
   int trabalhoLocal = 100 / numtasks;
   for (int i = rank * trabalhoLocal; i < (rank + 1) * trabalhoLocal; i++)
      produtoLocal = produtoLocal + vetor1[i] * vetor2[i];

   // Faz uma redução;
   MPI_Reduce(&produtoLocal, &produtoEscalar, 1, MPI_FLOAT, MPI_SUM, 0, MPI_COMM_WORLD);

   // Valor do Produto Escalar.
   printf("Rank %d: %0.2f\n", rank, produtoEscalar);
   
   // Finalizar o ambiente. 
   MPI_Finalize();
}

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
   int  numtasks, rank, len, rc; 

   // Inicializar o Ambiente.  
   MPI_Init(&argc,&argv);

   // Recuperar o número de Processos.
   MPI_Comm_size(MPI_COMM_WORLD, &numtasks);

   // Recuperar o identificador do processo.
   MPI_Comm_rank(MPI_COMM_WORLD, &rank);

   // Define um vetor local
   int vetorLocal[10];
   for (int i = 0; i < 10; i++)
      vetorLocal[i] = rank;
   
   // Este vetor precisa existir em todos os processos. 
   int *vetorFinal;
   vetorFinal = (int *) malloc(10 * numtasks * sizeof(int));
   
   MPI_Allgather(vetorLocal, 10, MPI_INT, vetorFinal, 10, MPI_INT, MPI_COMM_WORLD);

   char output[5000];
   sprintf(output, "Rank %d: ", rank);
   for (int i = 0; i < 10 * numtasks; i++) {
       sprintf(output, "%s%d ", output, vetorFinal[i]);
   }
   printf("%s\n", output);
  
   free(vetorFinal);
      
   // Finalizar o ambiente. 
   MPI_Finalize();
}

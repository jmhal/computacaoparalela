#include "mpi.h"
#include <stdio.h>

// Vamos criar um mesh 2D com 16 processos (4x4)
#define SIZE 16
#define UP    0
#define DOWN  1
#define LEFT  2
#define RIGHT 3

int main(int argc, char *argv[])  {

   // Informações do comunicador global
   int numtasks, rank, source, dest, outbuf, i, tag = 1; 
   int inbuf[4]={MPI_PROC_NULL,MPI_PROC_NULL,MPI_PROC_NULL,MPI_PROC_NULL,};

   // Dimensões
   int vizinhos[4], dims[2]={4,4};
   int periods[2]={1,1}, reorder=0, coords[2];
   
   // Controlar o fluxo de informações no mesh
   MPI_Request reqs[8];
   MPI_Status stats[8];

   // Comunicador para a topologia
   MPI_Comm cartcomm;   
   
   // Inicialização
   MPI_Init(&argc,&argv);
   MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
   
   if (numtasks == SIZE) {
      // Criar a Topologia
      MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods, reorder, &cartcomm);

      // Pegar o rank do processo no novo comunicador
      MPI_Comm_rank(cartcomm, &rank);

      // Pegar as coordenadas no processo no mesh
      MPI_Cart_coords(cartcomm, rank, 2, coords);

      // Guardar os vizinhos superior e inferior. 
      MPI_Cart_shift(cartcomm, 0, 1, &vizinhos[UP], &vizinhos[DOWN]);

      // Guardar os vizinhos a esquerda e a direita.
      MPI_Cart_shift(cartcomm, 1, 1, &vizinhos[LEFT], &vizinhos[RIGHT]);
   
      printf("rank= %d coordenadas= %d %d  vizinhos(u,d,l,r)= %d %d %d %d\n",
             rank,coords[0],coords[1],vizinhos[UP],vizinhos[DOWN],vizinhos[LEFT],
             vizinhos[RIGHT]);
   
      outbuf = rank;
   
      // Cada processo troca seu rank com os vizinhos diretos.
      for (i=0; i<4; i++) {
         dest = vizinhos[i];
         source = vizinhos[i];
         MPI_Isend(&outbuf, 1, MPI_INT, dest, tag, MPI_COMM_WORLD, &reqs[i]);
         MPI_Irecv(&inbuf[i], 1, MPI_INT, source, tag, MPI_COMM_WORLD, &reqs[i+4]);
      }
   
      MPI_Waitall(8, reqs, stats);
   
      printf("rank= %d inbuf(u,d,l,r)= %d %d %d %d\n",
             rank,inbuf[UP],inbuf[DOWN],inbuf[LEFT],inbuf[RIGHT]);  
   }
   else
      printf("São necessários %d processos.\n",SIZE);
   
   MPI_Finalize();
   return 0;
}



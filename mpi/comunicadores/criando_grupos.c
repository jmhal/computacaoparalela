#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>

#define NPROCS 8

int main(int argc, char *argv[])  {
   // Variáveis gerais do MPI
   int rank, new_rank, sendbuf, recvbuf, numtasks;
   
   // Vetores para direcionar a criação dos grupos.
   int ranks1[4]={0,1,2,3}, ranks2[4]={4,5,6,7};

   // Handle para os novos grupos e comunicador
   MPI_Group  orig_group, new_group;  
   MPI_Comm new_comm;   
   
   // Inicializa o ambiente.
   MPI_Init(&argc,&argv);
   MPI_Comm_rank(MPI_COMM_WORLD, &rank);
   MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
   
   // Vamos usar 8 processos no exemplo.
   if (numtasks != NPROCS) {
      printf("Esse programa só funciona com %d processos\n", NPROCS);
      MPI_Finalize();
      exit(0);
   }
   
   // Cada processo enviará seu próprio rank do grupo comunicador global.
   sendbuf = rank;
   
   // Recupera o handle do grupo original
   MPI_Comm_group(MPI_COMM_WORLD, &orig_group);
   
   // Divide os processos entre dois grupos de acordo com o rank.
   if (rank < NPROCS/2) {
      MPI_Group_incl(orig_group, NPROCS/2, ranks1, &new_group);
   }
   else {
      MPI_Group_incl(orig_group, NPROCS/2, ranks2, &new_group);
   }
   
   // Criar um novo comunicador e realizar operação coletiva.
   MPI_Comm_create(MPI_COMM_WORLD, new_group, &new_comm);
   MPI_Allreduce(&sendbuf, &recvbuf, 1, MPI_INT, MPI_SUM, new_comm);
   
   // Recuperar o rank no novo grupo.
   MPI_Group_rank (new_group, &new_rank);
   printf("rank= %d newrank= %d recvbuf= %d\n", rank, new_rank, recvbuf);
   
   MPI_Finalize();
}


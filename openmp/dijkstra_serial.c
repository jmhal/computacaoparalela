# include <stdlib.h>
# include <stdio.h>
# include <time.h>

# define NV 6

// Função geral que inicia o algoritmo.
int *dijkstra_distancia(int matrizDistancia[NV][NV]);

// Função que encontra o vizinho mais próximo do grupo já visitado.
void achar_vizinho_mais_proximo(int minimaDistancia[NV], int visitado[NV], int *d, int *v);

// Inicializa o matriz com as distâncias diretas
void inicializar(int matrizDistancia[NV][NV]);

// Atualiza as distâncias em relação ao vizinho adicionaddo.
void atualizar_distancia_minima(int mv, int visitado[NV], int matrizDistancia[NV][NV], int minimaDistancia[NV]);

int main(int argc, char **argv) {
   int i;
   int j;
   int infinito = 2147483647;
   int *minimaDistancia;
   int matrizDistancia[NV][NV];

   // Inicializar a matriz do Grafo.
   inicializar(matrizDistancia);
  
   printf("Matriz do Grafo:\n" );
   for ( i = 0; i < NV; i++ ) {
      for ( j = 0; j < NV; j++ ) {
         if (matrizDistancia[i][j] == infinito) {
            printf(" Inf ");
         } else {
            printf(" %3d ", matrizDistancia[i][j] );
         }
      }
      printf("\n");
   }
   
   // Chama o algoritmo
   minimaDistancia = dijkstra_distancia(matrizDistancia);
  
   printf("Distâncias mínimas a partir do vértice 0:\n");
   for (i = 0; i < NV; i++) {
      printf("%2d  %2d\n", i, minimaDistancia[i]);
   }

   // Finaliza
   free(minimaDistancia);
   return 0;
}

int *dijkstra_distancia(int matrizDistancia[NV][NV]) {
   // Vetor que irá indicar se o nó já foi visitado (1) ou não (0).
   int *visitado;

   // Vetor que irá armazenar a solução sendo construída.
   int *minimaDistancia;

   // A distância mínima encontrada em uma iteração.
   int md;

   // O vizinho mais próximo encontrado em um iteração.
   int mv;

   // Etapa do algoritmo. 
   int etapa; 
   int i;
   
   visitado = (int *) malloc(NV * sizeof(int));
   visitado[0] = 1;
   for (i = 1; i < NV; i++ ) {
      visitado[i] = 0;
   }
   
   minimaDistancia = (int *) malloc(NV * sizeof(int));
   for (i = 0; i < NV; i++) {
      minimaDistancia[i] = matrizDistancia[0][i];
   }
   
   for (etapa = 1; etapa < NV; etapa++) {
      achar_vizinho_mais_proximo(minimaDistancia, visitado, &md, &mv);

      if (mv == -1) {
         break;
      }

      visitado[mv] = 1;
      atualizar_distancia_minima(mv, visitado, matrizDistancia, minimaDistancia);
   }

   free(visitado);

   return minimaDistancia;
}

void achar_vizinho_mais_proximo(int minimaDistancia[NV], int visitado[NV], int *d, int *v) {
   int i;
   int infinito = 2147483647;

   *d = infinito;
   *v = -1;
   // Daqueles nós não visitados, qual mais próximo?
   for (i = 0; i < NV; i++) {
      if (!visitado[i] && minimaDistancia[i] < *d) {
         *d = minimaDistancia[i];
         *v = i;
      }
   }
   return;
}

void atualizar_distancia_minima(int mv, int visitado[NV], int matrizDistancia[NV][NV], int minimaDistancia[NV]) {
   int i;
   int infinito = 2147483647;
 
   // Atualizar a distância dos nós não visitados, visto que um novo nó está no grupo
   for (i = 0; i < NV; i++) {
      if (!visitado[i]) {
         if (matrizDistancia[mv][i] < infinito) {
            if (minimaDistancia[mv] + matrizDistancia[mv][i] < minimaDistancia[i]) {
               minimaDistancia[i] = minimaDistancia[mv] + matrizDistancia[mv][i];
            }
         }
      }
   }
   return;
}

void inicializar(int matrizDistancia[NV][NV]) {
   int i;
   int infinito = 2147483647;
   int j;

   for (i = 0; i < NV; i++) {
      for (j = 0; j < NV; j++) {
         if (i == j) {
            matrizDistancia[i][i] = 0;
         } else {
            matrizDistancia[i][j] = infinito;
         }
      }
   }

   matrizDistancia[0][1] = matrizDistancia[1][0] = 40;
   matrizDistancia[0][2] = matrizDistancia[2][0] = 15;
   matrizDistancia[1][2] = matrizDistancia[2][1] = 20;
   matrizDistancia[1][3] = matrizDistancia[3][1] = 10;
   matrizDistancia[1][4] = matrizDistancia[4][1] = 25;
   matrizDistancia[2][3] = matrizDistancia[3][2] = 100;
   matrizDistancia[1][5] = matrizDistancia[5][1] = 6;
   matrizDistancia[4][5] = matrizDistancia[5][4] = 8;
   return;
}




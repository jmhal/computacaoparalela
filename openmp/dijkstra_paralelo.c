#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <omp.h>

#define NV 6

int *dijkstra_distancia(int matrizDistancia[NV][NV]);
void achar_vizinho_mais_proximo(int inicio, int fim, int minimaDistancia[NV], int visitado[NV], int *d, int *v);
void inicializar(int matrizDistancia[NV][NV]);
void atualizar_distancia_minima(int inicio, int fim, int mv, int visitado[NV], int matrizDistancia[NV][NV], int minimaDistancia[NV]);

int main(int argc, char **argv) {
   int i;
   int infinito = 2147483647;
   int j;
   int *minimaDistancia;
   int matrizDistancia[NV][NV];

   // Inicializar a matriz do Grafo.
   inicializar(matrizDistancia);
  
   printf("Matriz do Grafo:\n" );
   for ( i = 0; i < NV; i++ ) {
      for ( j = 0; j < NV; j++ ) {
         if (matrizDistancia[i][j] == infinito) {
            printf(" Inf ");
         } else {
            printf(" %3d ", matrizDistancia[i][j] );
         }
      }
      printf("\n");
   }
   
   // Chama o algoritmo
   minimaDistancia = dijkstra_distancia(matrizDistancia);
  
   printf("Distâncias mínimas a partir do vértice 0:\n");
   for (i = 0; i < NV; i++) {
      printf("%2d  %2d\n", i, minimaDistancia[i]);
   }

   // Finaliza
   free(minimaDistancia);
   return 0;
}

int *dijkstra_distancia(int matrizDistancia[NV][NV]) {
   // Indica se o nó já foi analisado ou não. 
   int *visitado;

      int infinito = 2147483647;
   int i;
   
   // A distância mínima geral e a própria de cada thread.
   int md, minha_md;

   // O vizinho mais próximo geral e o encontrado por cada thread.
   int mv, meu_mv;
   
   // Vetor que armazena a solução sendo construída.
   int *minimaDistancia;

   // Variáveis para controlar o que cada thread faz.
   int minha_etapa; 
   int meu_inicio, meu_ultimo;
   
   // Número de threads e identificador próprio. 
   int nthreads, meu_id; 
   
   visitado = (int *) malloc(NV * sizeof(int));
   visitado[0] = 1;
   for (i = 1; i < NV; i++ ) {
      visitado[i] = 0;
   }
   
   minimaDistancia = (int *) malloc(NV * sizeof(int));
   for (i = 0; i < NV; i++) {
      minimaDistancia[i] = matrizDistancia[0][i];
   }

   # pragma omp parallel private (meu_inicio, meu_id, meu_ultimo, minha_md, meu_mv, minha_etapa) \
    shared (visitado, md, minimaDistancia, mv, nthreads, matrizDistancia)
   {
      meu_id = omp_get_thread_num();
      nthreads = omp_get_num_threads();
      meu_inicio = (meu_id * NV) / nthreads;
      meu_ultimo = ((meu_id + 1) * NV) / nthreads - 1;

      # pragma omp single 
      {
         printf("Thread %d: Região Paralela com %d threads.\n", meu_id, nthreads);
      }

      for (minha_etapa = 1; minha_etapa < NV; minha_etapa++) {
         # pragma omp single 
         {
            md = infinito;
            mv = -1;
         }
         achar_vizinho_mais_proximo(meu_inicio, meu_ultimo, minimaDistancia, visitado, &minha_md, &meu_mv);

         # pragma omp critical
         {
            if (minha_md < md) {
               md = minha_md;
               mv = meu_mv;
            }
         }

         // Vai garantir que todos os threads executaram a seção crítica acima. 
         # pragma omp barrier

         // Se o valor de mv for -1, o algoritmo já completou a matriz.
         // Caso contrário, marque o nó encontrado como o mais próximo como visitado.
         # pragma omp single
         { 
            if (mv != -1) {
               visitado[mv] = 1;
               printf("Thread %d: Conectando vértice %d.\n", meu_id, mv);
            }
         }

         // Esperar que todos os threads tenha certeza se a execução vai continuar
         // ou não. 
         # pragma omp barrier

         // Se o algoritmo continua, então atualizar para suas variáveis locais
         // os valores encontrados pelos outros threads.
         if (mv != -1) {
            atualizar_distancia_minima(meu_inicio, meu_ultimo, mv, visitado, matrizDistancia, minimaDistancia);
         }

         // Confirmar que todos os threads estão consistentes.
         # pragma omp barrier 
        
      }
      // Uma vez finalizado o laço, sair da região paralela.
      # pragma omp single
      {
         printf("\n");
         printf("Thread %d: Fim da Região Paralela.\n", meu_id);
      }
   }

   free(visitado);

   return minimaDistancia;
}

void achar_vizinho_mais_proximo(int inicio, int fim, int minimaDistancia[NV], int visitado[NV], int *d, int *v) {
   int i;
   int infinito = 2147483647;

   *d = infinito;
   *v = -1;
   for (i = inicio; i <= fim; i++) {
      if (!visitado[i] && minimaDistancia[i] < *d) {
         *d = minimaDistancia[i];
         *v = i;
      }
   }
   return;
}

void atualizar_distancia_minima(int inicio, int fim, int mv, int visitado[NV], int matrizDistancia[NV][NV], int minimaDistancia[NV]) {
   int i;
   int infinito = 2147483647;
 
   for (i = inicio; i <= fim; i++) {
      if (!visitado[i]) {
         if (matrizDistancia[mv][i] < infinito) {
            if (minimaDistancia[mv] + matrizDistancia[mv][i] < minimaDistancia[i]) {
               minimaDistancia[i] = minimaDistancia[mv] + matrizDistancia[mv][i];
            }
         }
      }
   }
   return;
}

void inicializar(int matrizDistancia[NV][NV]) {
   int i;
   int infinito = 2147483647;
   int j;

   for (i = 0; i < NV; i++) {
      for (j = 0; j < NV; j++) {
         if (i == j) {
            matrizDistancia[i][i] = 0;
         } else {
            matrizDistancia[i][j] = infinito;
         }
      }
   }

   matrizDistancia[0][1] = matrizDistancia[1][0] = 40;
   matrizDistancia[0][2] = matrizDistancia[2][0] = 15;
   matrizDistancia[1][2] = matrizDistancia[2][1] = 20;
   matrizDistancia[1][3] = matrizDistancia[3][1] = 10;
   matrizDistancia[1][4] = matrizDistancia[4][1] = 25;
   matrizDistancia[2][3] = matrizDistancia[3][2] = 100;
   matrizDistancia[1][5] = matrizDistancia[5][1] = 6;
   matrizDistancia[4][5] = matrizDistancia[5][4] = 8;
   return;
}




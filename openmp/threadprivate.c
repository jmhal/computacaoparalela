#include <omp.h> 
#include <stdio.h>

// Variáveis globais
int  a, b, i, tid;
float x;

// Para quaisquer regiões paralelas, a e x terão cópias locais
#pragma omp threadprivate(a, x)
 
int main(int argc, char *argv[]) {
   
   // threadprivate só funciona com número constante de threads.
   omp_set_dynamic(0);
 
   printf("Primeira região paralela:\n");
   #pragma omp parallel private(b, tid)
   {
      tid = omp_get_thread_num();
      a = tid;
      b = tid;
      x = 1.1 * tid +1.0;
      printf("Thread %d: a, b, x= %d %d %f\n", tid, a, b, x);
   }  // Fim da região paralela.
 
   printf("**************************************\n");
   printf("Algum trabalho serial do master thread\n");
   printf("**************************************\n");
 
   printf("Segunda região paralela:\n");
   #pragma omp parallel private(tid)
   {
      tid = omp_get_thread_num();
      printf("Thread %d: a, b, x= %d %d %f\n", tid, a, b, x);
   }  // Fim da região paralela.

   return 0;
 }

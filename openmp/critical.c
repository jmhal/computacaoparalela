#include <omp.h>
#include <stdio.h>

int  main(int argc, char *argv[]) {
   int x, y;
   x = 0;
   y = 1;

   #pragma omp parallel shared(x) 
   {

      #pragma omp critical (atualizax)
      x = x + 1;

      // Todos os threads executam sem controle
      y = y * x;

      #pragma omp critical (atualizax)
      x = x + 1;

   }  /* Fim da região paralela */

   printf("x: %d, y:%d\n", x, y);
   return 0;
}


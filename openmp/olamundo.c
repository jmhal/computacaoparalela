#include <stdio.h>
#include <omp.h>

int  main(int argc, char *argv[]) {

   int nthreads, tid;

   // Cria um time de threads, cada um com sua variável tid privada.
   #pragma omp parallel private(tid)
   {
      // Recupera o identificador de cada thread.
      tid = omp_get_thread_num();
      printf("Olá mundo do thread = %d\n", tid);
   
      // Apenas o mestre faz isto.
      if (tid == 0) {
         nthreads = omp_get_num_threads();
          printf("Número de threads = %d\n", nthreads);
      }
   
   }  // Todos os threads, fora o master, finalizam.

   printf("Olá, eu sou o mestre %d.\n", tid);
   return 0;
}



#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
   long int i, chunk;
   long int N = 10000000;
   float *a, *b, *c;
   a = (float *) malloc(N * sizeof(float));
   b = (float *) malloc(N * sizeof(float));
   c = (float *) malloc(N * sizeof(float));
   
 
   // Inicialização
   for (i = 0; i < N; i++) {
      a[i] = b[i] = 1.0;
      //if (i < 100)
      //   printf("%.1f ", a[i]);
   }
   //chunk = CHUNKSIZE;
   //printf("\n");
   #pragma omp parallel shared(a,b,c,chunk) private(i)
   {
     #pragma omp for schedule(static) nowait
     for (i = 0; i < N; i++) {
       //int tid = omp_get_thread_num();
       c[i] = a[i] * b[i];
       //printf("Thread %d: executando iteração %d, a[i] = %.1f\n", tid, i, a[i]);
     }  
  
   }   // Fim da região paralela.
/*
   printf("\n");
   printf("\n");
   for (i = 0; i < N; i++) 
      printf("%.1f ", c[i]);
   printf("\n");
*/
   free(a);
   free(b);
   free(c);
   return 0;
}
